from datetime import datetime
# current date and time
# Returns a datetime object containing the local date and time
dateTimeObj = datetime.now()
print(dateTimeObj)
# Converting datetime object to string
dateTimeObj = datetime.now()
timestampStr = dateTimeObj.strftime("%d-%b-%Y (%H:%M:%S.%f)")
print('Current Timestamp : ', timestampStr)


firstStr = "sample"
secStr   = "sample"
firstStr = "sample"
secStr   = "sample"
if firstStr == secStr:
    print('Both Strings are same')
else:
    print('Strings are not same')

if firstStr.startswith('sa'):
    print('testvalid')

else:
    print('invalid test')

#compound assignment operator
num1 = 4
num2 = 5
res = num1 + num2
res += num1
print ("Line 1 - Result of + is ", res)
res +=num1
print(num2)