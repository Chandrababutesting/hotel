import array as arr

a = arr.array( 'i', [1, 2, 3] )

# printing original array
print( "The new created array is : ", end=" " )
for i in range( 0, 2 ):
    print( a[i], end=" " )
print()

# creating an array with float type
b = arr.array( 'd', [2.5, 3.2, 3.3] )

# printing original array
print( "The new created array is : ", end=" " )
for i in range( 0, 1 ):
    print( b[i], end=" " )

l = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

a = arr.array( 'i', l )
print( "Intial Array: " )
for i in (a):
    print( i, end=" " )

# Print elements of a range
# using Slice operation
Sliced_array = a[3:8]
print( "\nSlicing elements in a range 3-8: " )
print( Sliced_array )
Sliced_array = a[5:]
print( "\nElements sliced from 5th "
       "element till the end: " )
print( Sliced_array )

# Printing elements from
# beginning till end
Sliced_array = a[:]
print( "\nPrinting all elements using slice operation: " )
print( Sliced_array )

import array

# initializing array with array values
# initializes array with signed integers
arr = array.array( 'i', [1, 2, 3, 1, 2, 5] )

# printing original array
print( "The new created array is : ", end="" )
for i in range( 0, 6 ):
    print( arr[i], end=" " )

print( "\r" )

# using index() to print index of 1st occurrenece of 2
print( "The index of 1st occurrence of 2 is : ", end="" )
print( arr.index( 2 ) )

# using index() to print index of 1st occurrenece of 1
print( "The index of 1st occurrence of 1 is : ", end="" )
print( arr.index( 1 ) )

arr = array.array( 'i', [1, 2, 3, 1, 2, 5] )

# printing original array
print( "Array before updation : ", end="" )
for i in range( 0, 6 ):
    print( arr[i], end=" " )

print( "\r" )

# updating a element in a array
arr[2] = 6
print( "Array after updation : ", end="" )
for i in range( 0, 6 ):
    print( arr[i], end=" " )
print()

# updating a element in a array
arr[4] = 8
print( "Array after updation : ", end="" )
for i in range( 0, 6 ):
    print( arr[i], end=" " )

test_tup = (7, 8, 9, 1, 10, 7)

# printing original tuple
print( "The original tuple is : " + str( test_tup ) )

# Tuple elements inversions
# Using list() + sum()
res = sum( list( test_tup ) )

# printing result
print( "The summation of tuple elements are : " + str( res ) )

test_tup = ([7, 8], [9, 1], [10, 7])

# printing original tuple
print( "The original tuple is : " + str( test_tup ) )
esw=sum(list(str( test_tup ) ))
print(esw)

# Tuple elements inversions
# Using map() + list() + sum()
res = sum( list( map( sum, list( test_tup ) ) ) )

# printing result
print( "The summation of tuple elements are : " + str( res ) )



