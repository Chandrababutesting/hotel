import datetime
import random
import time

import pandas
from Screenshot import Screenshot_Clipping
from loguru import logger
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select

logger.info( 'test' )
ob = Screenshot_Clipping.Screenshot()
driver = webdriver.Chrome()
actions = ActionChains( driver )
global window_before
global window_after


def windowshandle():
    """window_after = driver.window_handles[1]
        driver.switch_to.window(window_after)
        logger.info('Switched to window')"""
    handles = driver.window_handles
    size = len( handles )
    for x in range( size ):
        driver.switch_to.window( handles[x] )
        logger.info( driver.title )


def windowshandle1():
    handles = driver.window_handles
    size = len( handles )
    for x in range( size ):
        if handles[x] != driver.current_window_handle:
            driver.switch_to.window( handles[x] )
            logger.info( driver.title )


def windowshandle2():
    handles = driver.window_handles
    size = len( handles )
    parent_handle = driver.current_window_handle
    for x in range( size ):
        if handles[x] != parent_handle:
            driver.switch_to.window( handles[x] )
            logger.info( driver.title )
            driver.close()
            break

    driver.switch_to.window( parent_handle )


def randomdate():
    start_date = datetime.date( 2020, 4, 30 )
    end_date = datetime.date( 2020, 8, 30 )
    time_between_dates = end_date - start_date
    days_between_dates = time_between_dates.days
    random_number_of_days = random.randrange( days_between_dates )
    random_date = start_date + datetime.timedelta( days=random_number_of_days )
    print( random_date.strftime( '%d-%m-%Y' ) )
    print( random_date )


def pageload():
    try:
        driver.execute_script( "window.scrollTo(0, document.body.scrollHeight);" )
        time.sleep( 2 )
    except TimeoutException:
        logger.info( "Timed out waiting for page to load" )
    finally:
        logger.info( "Page loaded" )


def applicationlogin(app, customer):
    # driver = webdriver.Chrome()

    excel_data_df = pandas.read_excel(
        'E:\\OTAProject\\FlightsTestData\\UserManagement\\' + app + '\\Userdetails\\UserDetails.xls',
        sheet_name='Sheet1' )
    try:
        driver.maximize_window()
    except:
        print( 'driver is not opened' )
    driver.get( excel_data_df['ApplicationURL'].iloc[0] )
    pageload()
    driver.find_element_by_id( 'sign_in_username_email' ).send_keys( excel_data_df['Username'].iloc[0] )
    driver.find_element_by_id( 'sign_in_password' ).send_keys( excel_data_df['Password'].iloc[0] )
    driver.find_element_by_xpath('//*[@id="srm-log-frm"]/div[3]/div[2]/a').click()
    time.sleep( 5 )
    # pageload()

    if customer == 'CCustomer':
        customers = driver.find_elements_by_css_selector( '.user-image' )
        customers[2].click()
        driver.find_element_by_xpath( './/*[@id="request_form"]/div/div/div[1]/span/span[1]/span/span[2]' ).click()
        driver.find_element_by_xpath( 'html/body/span/span/span[1]/input' ).send_keys(
            excel_data_df['CustomerName'].iloc[1] )
        time.sleep( 12 )
        driver.find_element_by_xpath( 'html/body/span/span/span[1]/input' ).send_keys( Keys.ENTER )
        time.sleep( 5 )
        pageload()
        element = driver.find_element_by_css_selector( '.btn.btn-lightblue.pull-right.requestsubmit' )
        driver.execute_script( "arguments[0].click();", element )
    else:
        customers1 = driver.find_elements_by_css_selector( '.fa.fa-user-circle' )
        customers1[0].click()
        time.sleep( 15 )
        driver.find_element_by_id( 'atu_mobile' ).send_keys( str( excel_data_df['Numbers'].iloc[1] ) )
        driver.find_element_by_xpath( '//button[contains(.," Search ")]' ).click()
        time.sleep( 7 )
        driver.find_element_by_xpath( '//input[@class="radio rc"]' ).click()
        time.sleep( 5 )
        driver.find_element_by_xpath( '//button[@id="rusubmit"]' ).click()
        time.sleep( 12 )
        # pageload()


def Hotelsearch(app1, customer1):
    window_before = driver.window_handles[0]
    driver.find_element_by_css_selector( '.fa.fa-hotel' ).click()
    # driver.find_element_by_xpath('//*[@ id ="accordion"]/div/ul/li[2]/a').click()
    print( 'clicked hotel test' )
    time.sleep( 4 )
    pageload()
    time.sleep( 9 )
    # window_after = driver.window_handles[1]
    # driver.switch_to_window(window_after)
    # driver.switch_to_window()
    # driver.get_window_position(window_after)
    excel_data_df = pandas.read_excel(
        'E:\\OTAProject\\HotelsTestData\\HotelsSearch\\' + app1 + '\\Hotelssearchdata.xls', sheet_name='Sheet1' )
    city = excel_data_df['City'].iloc[1]
    wb = driver.find_element_by_css_selector( "#search_city_hotel_1" )
    time.sleep( 3 )
    driver.execute_script( "arguments[0].value='" + city + "';", wb )
    driver.find_element_by_css_selector( '#search_city_hotel_1' ).click()
    time.sleep( 2 )
    logger.info( 'clickedsuccessfuly' )
    driver.find_element_by_css_selector( '#search_city_hotel_1' ).send_keys( Keys.SPACE )
    time.sleep( 2 )
    driver.find_element_by_css_selector( '#search_city_hotel_1' ).send_keys( Keys.SPACE )
    time.sleep( 2 )
    driver.find_element_by_css_selector( '#search_city_hotel_1' ).send_keys( Keys.BACK_SPACE )
    time.sleep( 2 )
    driver.find_element_by_css_selector( '#search_city_hotel_1' ).send_keys( Keys.BACK_SPACE )
    time.sleep( 2 )
    driver.find_element_by_css_selector( '#search_city_hotel_1' ).send_keys( Keys.BACK_SPACE )
    time.sleep( 2 )
    driver.find_element_by_css_selector( '#search_city_hotel_1' ).send_keys( Keys.ARROW_DOWN )
    time.sleep( 3 )
    driver.find_element_by_css_selector( '#search_city_hotel_1' ).send_keys( Keys.ENTER )
    time.sleep( 3 )
    driver.find_element_by_css_selector( '#select2-hotel_nationality_1-container' ).click()
    start_date = datetime.date( 2020, 4, 30 )
    end_date = datetime.date( 2020, 8, 30 )
    time_between_dates = end_date - start_date
    days_between_dates = time_between_dates.days
    random_number_of_days = random.randrange( days_between_dates )
    random_date = start_date + datetime.timedelta( days=random_number_of_days )
    date = driver.find_element_by_id( 'hotel_check_in_1' )
    driver.execute_script( "arguments[0].value='" + random_date.strftime( '%d-%m-%Y' ) + "';", date )
    time.sleep( 3 )
    pageload()
    date1 = driver.find_element_by_css_selector( '#hotel_check_in_1' )
    driver.execute_script( "arguments[0].click();", date1 )
    # driver.find_element_by_id('hotel_check_in_1').click()
    time.sleep( 3 )
    logger.info( 'clickedelement' )
    # wbp = driver.find_element_by_css_selector('.ui-state-active')
    # driver.execute_script("arguments[0].click();",wbp)
    nights = excel_data_df['Nights'].iloc[1]
    driver.find_element_by_css_selector( '#hotel_no_of_nights_1' ).click()
    driver.find_element_by_css_selector( '#hotel_no_of_nights_1' ).clear()
    wb_nights = driver.find_element_by_css_selector( 'input[id="hotel_no_of_nights_1"][name="hotel_no_of_nights_[1]"]' )
    driver.execute_script( "arguments[0].value='" + str( nights ) + "';", wb_nights )
    time.sleep( 2 )
    wb_nights.click()
    wb_nights.send_keys( Keys.SPACE )
    time.sleep( 2 )
    rooms = excel_data_df['Rooms'].iloc[0]
    driver.find_element_by_css_selector( '#select2-hotel_room_count_1-container' ).click()
    time.sleep( 2 )
    rooms_count = driver.find_elements_by_css_selector( '.select2-search__field' )
    rooms_count[2].send_keys( str( rooms ) )
    time.sleep( 2 )
    rooms_count[2].send_keys( Keys.BACK_SPACE )
    rooms_count[2].send_keys( Keys.BACK_SPACE )
    time.sleep( 2 )
    rooms_count[2].send_keys( Keys.ENTER )
    romm_int = int( round( rooms ) )
    logger.info( romm_int )

    i = 1
    count = 2
    for i in range( 0, romm_int ):
        axpath = "hotel_room_adult_count_1_"
        xpath = axpath + str( i + 1 )
        logger.info( xpath )
        select = Select( driver.find_element_by_id( 'hotel_room_adult_count_1_' + str( i + 1 ) + '' ) )
        select.select_by_visible_text( "3" )
        select = Select( driver.find_element_by_id( 'hotel_room_child_count_1_' + str( i + 1 ) + '' ) )
        select.select_by_visible_text( "2" )
        select = Select( driver.find_element_by_id( 'hotel_room_child_age_1_1_' + str( i ) + '' ) )
        select.select_by_visible_text( "3" )
        try:
            select = Select( driver.find_element_by_id( 'hotel_room_child_age_1_2_' + str( i - 1 ) + '' ) )
            select.select_by_visible_text( "2" )
            select = Select( driver.find_element_by_id( 'hotel_room_child_age_1_2_' + str( i ) + '' ) )
            select.select_by_visible_text( "2" )
        except:
            logger.info( 'details are not displaying' )

    time.sleep( 4 )
    driver.find_element_by_css_selector( '.fa.fa-info-circle.fa-lg' ).click()
    pageload()
    time.sleep( 4 )
    markuptye = excel_data_df['Markuptype'].iloc[0]
    logger.info( 'markuptye==' + markuptye )
    select = Select( driver.find_element_by_css_selector( '#hotel_markup_type_1' ) )
    select.select_by_visible_text( str( markuptye ) )
    time.sleep( 4 )
    driver.find_element_by_css_selector( '#hotel_extramarkup_1' ).clear()
    markupvalue = excel_data_df['Markup-per-value'].iloc[0]
    time.sleep( 4 )
    markupvalue_wb = driver.find_element_by_css_selector( '#hotel_extramarkup_1' )
    driver.execute_script( "arguments[0].value='" + str( markupvalue ) + "';", markupvalue_wb )
    driver.find_element_by_xpath( '//button[@value="1"]' ).click()
    pageload()
    time.sleep( 5 )
    Extramarkup = driver.find_element_by_css_selector( '#hotel_extramarkup_1' ).get_attribute( "value" )
    time.sleep( 3 )
    markuptype_p_a = driver.find_element_by_css_selector( '#hotel_markup_type_1' ).get_attribute( "value" )
    time.sleep( 3 )
    driver.find_element_by_xpath( '(//button[@value="1"])[2]' ).click()
    time.sleep( 7 )
    try:
        driver.find_element_by_css_selector( '.btn.btn-lightblue.yes' ).click()
    except:
        logger.info( 'details are not displaying' )
    time.sleep( 10 )
    windowshandle()
    pageload()
    time.sleep( 20 )


# window_before = driver.window_handles[0]
# driver.find_element_by_id('btn_quote_mul').click()


def supplierselection(app1, customer1):
    window_after = driver.window_handles[1]
    pageload()
    time.sleep( 10 )
    # switch on to new child window
    driver.switch_to.window( window_after )
    excel_data_df = pandas.read_excel(
        'E:\\OTAProject\\HotelsTestData\\HotelsSearch\\' + app1 + '\\Hotelssearchdata.xls', sheet_name='Sheet1' )
    supplier1 = driver.find_elements_by_css_selector( '.mt-checkbox.apicheckbox' )
    suppliernumber = excel_data_df['Suppliernumber'].iloc[1]
    suppliernumber_int = int( round( suppliernumber ) )
    time.sleep( 3 )
    driver.execute_script( "arguments[0].scrollIntoView()", supplier1[0] )
    pageload()
    time.sleep( 10 )
    driver.execute_script( "arguments[0].click();", supplier1[0] )
    pageload()
    time.sleep( 3 )
    time.sleep( 15 )
    driver.execute_script( "arguments[0].scrollIntoView()", supplier1[0] )
    time.sleep( 5 )
    pageload()
    time.sleep( 5 )
    hotelsnames = driver.find_elements_by_css_selector( '.showmorebtn.show_more_btn' )
    hotelsnames_count = len( hotelsnames )
    pageload()
    time.sleep( 5 )
    hotelsnames[0].click()
    pageload()
    time.sleep( 4 )
    driver.execute_script( "arguments[0].scrollIntoView()", hotelsnames[0] )
    pageload()
    time.sleep( 12 )
    hotelattributecode = hotelsnames[0].get_attribute( 'hotelcode' )
    hotel_roomcount = driver.find_elements_by_css_selector(
        'form[id="hotel_online_quote_frm_' + hotelattributecode + '"] [class="bookbtns"]' )
    hotel_roomcount[0].click()
    pageload()
    time.sleep( 5 )
    discountType = excel_data_df['Discounttype'].iloc[0]
    select = Select( driver.find_element_by_css_selector( '#discountType' ) )
    select.select_by_visible_text( str( discountType ) )
    time.sleep( 3 )
    driver.find_element_by_css_selector( '#discount' ).clear()
    discountvalue = excel_data_df['Discount-per-value'].iloc[0]
    discountvalue_wb = driver.find_element_by_css_selector( '#discount' )
    driver.execute_script( "arguments[0].value='" + str( discountvalue ) + "';", discountvalue_wb )
    driver.find_element_by_css_selector( '#discount' ).click()
    time.sleep( 2 )
    driver.find_element_by_css_selector( '#discount' ).send_keys( Keys.TAB )
    time.sleep( 2 )
    WebElement = driver.find_element_by_xpath( '//*[@id="Room 1"]/div/div[1]/div[7]/button[1]/i' )
    driver.execute_script( "arguments[0].scrollIntoView()", WebElement )
    pageload()
    time.sleep( 5 )
    driver.execute_script( "arguments[0].scrollIntoView()", WebElement )
    time.sleep( 5 )
    wb1 = driver.find_element_by_id( 'titlea11' )
    driver.execute_script( "arguments[0].value='Mr.';", wb1 )
    time.sleep( 9 )
    driver.find_element_by_id( 'firstnamea11' ).clear()
    driver.find_element_by_id( 'firstnamea11' ).send_keys( 'Chandrbabu' )
    driver.find_element_by_id( 'lastnamea11' ).clear()
    driver.find_element_by_id( 'lastnamea11' ).send_keys( 'guntur' )
    time.sleep( 4 )
    driver.find_element_by_xpath( '//*[@id="Room 1"]/div/div[1]/div[7]/button[1]/i' ).click()
    time.sleep( 14 )
    try:
        for i in range( 0, 10 ):
            axpath = "titlec1"
            xpath = axpath + str( i + 1 )
            driver.find_element_by_id( xpath ).send_keys( 'Miss.' )
    except:
        logger.info( 'less rooms are there ' )
    try:
        for i in range( 0, 10 ):
            axpath = "titlec2"
            xpath = axpath + str( i + 1 )
            driver.find_element_by_id( xpath ).send_keys( 'Miss.' )
    except:
        logger.info( 'less rooms are there ' )
    driver.find_element_by_id( 'user_email' ).clear()
    driver.find_element_by_id( 'user_email' ).send_keys( 'chandrababu@gmail.com' )
    driver.find_element_by_id( 'user_mobile' ).clear()
    driver.find_element_by_id( 'user_mobile' ).send_keys( '9866172799' )
    time.sleep( 3 )
    WebElement = driver.find_element_by_css_selector( '.itienarySub.showmorebtn.pull-right' )
    driver.execute_script( "arguments[0].scrollIntoView()", WebElement )
    time.sleep( 3 )
    try:
        driver.find_element_by_css_selector( '#richText-sjhyo' ).click()
        time.sleep( 3 )
        driver.find_element_by_css_selector( '#richText-sjhyo' ).send_keys( 'Rooms Booked Successfully' )
    except:
        logger.info( "Rooms booking is not validated" )
    e1l = driver.find_element_by_id( 'accept' )
    driver.execute_script( "arguments[0].click();", e1l )
    logger.info( 'webelement not clicked successfully' )
    time.sleep( 5 )
    e12 = driver.find_element_by_css_selector( '.itienarySub.showmorebtn.pull-right' )
    driver.execute_script( "arguments[0].click();", e12 )
    logger.info( 'clicked successfully next page ' )
    time.sleep( 9 )
    e13 = driver.find_element_by_css_selector( '.itienarySub.showmorebtn.pull-right' )
    driver.execute_script( "arguments[0].click();", e13 )
    pageload()
    time.sleep( 9 )
    bookingconfirmation = driver.find_element_by_xpath(
        '//*[@id="idaptbtnsummary"]/div[2]/div/div/span[2]' ).get_attribute( 'value' )
    logger.info( 'bookingconfirmation' )
    time.sleep( 3 )
    try:
        votured = driver.find_element_by_xpath( '//*[@id="UpdatePanel"]/div/div[2]/a[1]' )
        driver.execute_script( "arguments[0].click();", votured )
        time.sleep( 3 )
        driver.switch_to.alert.accept()
    except:
        logger.info( 'Votured already ....' )
    time.sleep( 15 )


def signout():
    time.sleep( 9 )
    WebElement = driver.find_element_by_xpath( '/html/body/div[2]/div/div/div[2]/div[2]/ul/li/a/span[1]' )
    driver.execute_script( "arguments[0].scrollIntoView()", WebElement )
    pageload()
    time.sleep( 3 )
    signout = driver.find_element_by_xpath( '/html/body/div[2]/div/div/div[2]/div[2]/ul/li/a/span[1]' )
    signout.click()
    time.sleep( 3 )
    print( 'Signoutclickedsuccessfully' )
    time.sleep( 3 )
    handles = driver.window_handles
    size = len( handles )
    parent_handle = driver.current_window_handle
    for x in range( size ):
        if handles[x] != parent_handle:
            driver.switch_to.window( handles[x] )
            print( driver.title )
            driver.close()
            break

    driver.switch_to.window( parent_handle )
    time.sleep( 5 )
    signout1 = driver.find_element_by_xpath( '/html/body/div[2]/div/div/div[2]/div[2]/ul/li/ul/li/a' )
    signout1.click()
    time.sleep( 15 )
    driver.close()
    print( 'driver closed successfuly' )
    time.sleep( 3 )
    try:
        pageload()
    except:
        print( 'page not loaded' )
        time.sleep( 13 )
        # windowshandle2()
    #  driver.switch_to.window( window_before )
    # window_after = driver.window_handles[0]
    """driver.switch_to.window[0]
    #driver.switch_to_window(window_after)
    time.sleep(13)
    signout11 = driver.find_element_by_xpath('/html/body/div[1]/header/nav/div/ul/li[13]/a/span')
    signout11.click()
    time.sleep(5)
    webelementss=driver.find_elements_by_css_selector('.btn.btn-default.btn-flat')
    webelementss[1].click()
    driver.close()"""


def roomsselection(app1, environment, true=None):
    time.sleep( 15 )
    excel_data_df = pandas.read_excel(
        'E:\\OTAProject\\HotelsTestData\\HotelsSearch\\' + app1 + '\\Hotelssearchdata.xls', sheet_name='Sheet1' )
    hotelsnames = driver.find_elements_by_css_selector( '.showmorebtn.show_more_btn' )
    hotelsnames_count = len( hotelsnames )
    hotelsnames[0].click()
    temp = 1
    for i in range( 0, hotelsnames_count ):
        if temp == 0:
            break
        hotel_status_rf_norf = excel_data_df['Hotelstatus'].iloc[1]
        sc = hotelsnames_count + 1
        hotelattributecode = hotelsnames.get( i ).get_attribute( 'hotelcode' )
        if hotelsnames.get( i ).getText().equals( 'More Rooms' ):
            hotelsnames.get( i ).click()
        pageload()
        time.sleep( 2 )
        Suppliertext = excel_data_df['SupplierName'].iloc[1]
        suppliernumber = excel_data_df['Suppliernumber'].iloc[1]
        if str( suppliernumber ).startswith( '0' ) == true:
            name = 'bookbtns'
        else:
            name = 'bookbtn'
        can2 = driver.find_elements_by_css_selector(
            'form[id="hotel_online_quote_frm_' + hotelattributecode + '] [ class=' + name + ']' )
        orom = len( can2 )
        time.sleep( 2 )
        for i in range( 0, orom ):
            element = driver.find_elements_by_css_selector(
                'form[id="hotel_online_quote_frm_' + hotelattributecode + '] [class="rfndlbl"]' )
            roomstatusa = driver.find_elements_by_css_selector(
                'form[id="hotel_online_quote_frm_' + hotelattributecode + '] [class="show_status"]' )
            # if
            element[i].text( 'Available' ) and roomstatusa[i].text( 'Available' )
        # (hotel_status_rf_norf) & & roomstatusa.get(roomstatusa ).getText().equalsIgnoreCase( "Available" ))
        time( 3 )
        # element.get[0].getText().equalsIgnoreCase(hotel_status_rf_norf) & & roomstatusa.get(roomstatus ).getText().equalsIgnoreCase( "Available" ))
        element[i].text( 'Available' ) and roomstatusa[i].text( 'Available' )
        time( 3 )
    """try:
        hotel_roomcount=driver.find_elements_by_css_selector('form[id="hotel_online_quote_frm_'+hotelattributecode+'] [class="bookbtns"]')
        hotel_roomcount.get(roomstatus).click()
    Except:
        hotel_roomcount = driver.find_elements_by_css_selector('form[id="hotel_online_quote_frm_'+hotelattributecode+'] [class="bookbtn"]')
        hotel_roomcount.get( roomstatus ).click()

time.sleep(5)
time.sleep(5)
print('Welcome to good testing')
temp = 0
time.sleep(5)
break

if (temp == 1)
orom=0
// hotels += 1;
continue
else
break"""


applicationlogin( 'Dev', 'RCustomer' )
time.sleep( 5 )
Hotelsearch( 'Dev', 'customer' )
