"""""def sum_digits_string(str1):

    sum_digit = 0
    for x in str1:
        if x.isdigit() == True:
            z = int( x )
            sum_digit = sum_digit + z

    return sum_digit


print( sum_digits_string( "123abcd45" ) )
print( sum_digits_string( "abcd1234" ) )"""

a = "Hello, World!"
print(a[1])
##based on index the value is coming as e
#Slicing
b = "testing"
print(b[2:5])

c = "testing"
print(c[-5:-2])

d = "Hello, World!"
print(len(d))
#The strip() method removes any whitespace from the beginning or the end:
e = " Hello, World! "
print(e.strip())

f = "Hello, World!"
print(f.lower())


g = "Hello, World!"
print(g.upper())


h = "Hello, World!"
print(h.replace("H", "J"))

i = "Hello, World!"
print(i.split(",")) # returns ['Hello', ' World!']

txt = "The rain in Spain stays mainly in the plain"
x = "ain" in txt
print(x)
#This example returns the items from index -4 (included) to index -1 (excluded)
thislist = ["apple", "banana", "cherry", "orange", "kiwi", "melon", "mango"]
print(thislist[-4:-1])

"""open_list = ["[", "{", "("]
close_list = ["]", "}", ")"]"""


def check(my_string):
    brackets = ['()', '{)', '[]']
    while any( x in my_string for x in brackets ):
        for br in brackets:
            my_string = my_string.replace( br, '' )
    return not my_string


# Driver code
string = "{[]{()}}"
print( string, "-", "Balanced"
if check( string ) else "Unbalanced" )



# Function to check parentheses
"""def check(myStr):
    stack = []
    for i in myStr:
        if i in open_list:
            stack.append( i )
        elif i in close_list:
            pos = close_list.index( i )
            if ((len( stack ) > 0) and
                    (open_list[pos] == stack[len( stack ) - 1])):
                stack.pop()
            else:
                return "Unbalanced"
    if len( stack ) == 0:
        return "Balanced"
    else:
        return "Unbalanced"
    string1 = "{[]{()}}"
    print( string1, "-", check( string1 ) )

    string2 = "[{}{})(]"
    print( string2, "-", check( string2 ) )

    string3 = "((()"
    print( string3, "-", check( string3 ) )"""


def moveZerosToEnd(nums):
    j = 0

    for i in range( len( nums ) ):
        if nums[i] != 0 and nums[j] == 0:
            nums[i], nums[j] = nums[j], nums[i]
        if nums[j] != 0:
            j += 1


# Function to print the array elements
def printArray(arr, n):
    for i in range( 0, n ):
        print( arr[i], end=" " )
 # Driver program to test above
arr = [8, 9, 0, 1, 2, 0, 3]
n = len( arr )
print( "Original array:", end=" " )
printArray( arr, n )
moveZerosToEnd( arr )
print( "\nModified array: ", end=" " )
printArray( arr, n )