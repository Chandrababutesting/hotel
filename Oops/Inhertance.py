class employee():
    def __init__(self,name,age,id,salary):  #creating a function
        self.name = name  #self is an instance of a class
        self.age = age
        self.salary = salary
        self.id = id
emp1 = employee("harshit",22,1000,1234) #creating objects
emp2 = employee("arjun",23,2000,2234)
print(emp1.__dict__)#Prints dictionary

"""Explanation: ’emp1′ and ’emp2′ are the objects that are instantiated against the class
 ’employee’.Here, the word (__dict__) is a “dictionary” which prints all the values of object
  ‘emp1’ against the given parameter (name, age, salary).(__init__) acts like a constructor that 
  is invoked whenever an object is created."""
"""Inheritance
Polymorphism
Encapsulation
Abstraction"""

"""Single Inheritance:
Single level inheritance enables a derived class to inherit  characteristics from a single parent class. """


class employee1(): #  This is a parent This is a parent class

def __init__(self, name, age, salary):
self.name = name
self.age = age
self.salary = salary

class childemployee(employee1)://This is a child class
def __init__(self, name, age, salary,id):
self.name = name
self.age = age
self.salary = salary
self.id = id
emp1 = employee1('harshit',22,1000)
print(emp1.age)

Output: 22

Explanation:
I am taking the parent class and created a constructor (__init__),
class itself is initializing the attributes with parameters(‘name’, ‘age’ and ‘salary’).


Created
a
child


class ‘childemployee’ which is inheriting the properties from a parent class and finally instantiated objects ’emp1′ and ’emp2′ against the parameters.


Finally, I
have
printed
the
age
of
emp1.Well, you
can
do
a
hell
lot
of
things
like
print
the
whole
dictionary or name or salary.