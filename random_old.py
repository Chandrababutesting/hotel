import random
import string
import uuid
from loguru import logger

# string="aadadas12365875UTUTUJMJGWRQ$#%#$^#"
def randomString(stringLength=10):
    """Generate a random string of fixed length """
    # letters = string.ascii_lowercase
    letters = string.ascii_letters
    return ''.join(random.choice(letters) for i in range(stringLength))


logger.info(randomString())


def my_random_string(string_length=10):
    """Returns a random string of length string_length."""

    # Convert UUID format to a Python string.
    random = str(uuid.uuid4())

    # Make all characters uppercase.
    random = random.upper()

    # Remove the UUID '-'.
    random = random.replace("-", "")

    # Return the random string.
    return random[0:string_length]


logger.info(my_random_string(6))


def randString(length=5):
    # put your letters in the following string
    your_letters = 'abcdefghiABCVNMKLOPU1234567890'
    return ''.join((random.choice(your_letters) for i in range(length)))


logger.info("Random String with specific letters ", randString())
logger.info("Random String with specific letters ", randString(5))


def randomStringDigits(stringLength=6):
    """Generate a random string of letters and digits """
    lettersAndDigits = string.ascii_letters + string.digits
    return ''.join(random.choice(lettersAndDigits) for i in range(stringLength))


logger.info("Generating a Random String including letters and digits")
logger.info("First Random String is  ", randomStringDigits(8))
logger.info("Second Random String is ", randomStringDigits(8))
logger.info("Third Random String is  ", randomStringDigits(8))