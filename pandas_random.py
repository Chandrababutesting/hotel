import random
import string
import uuid
from loguru import logger


class Random_test():
    #string="aadadas12365875UTUTUJMJGWRQ$#%#$^#"
    def randomString(self, stringLength=10):
        """Generate a random string of fixed length """
        #letters = string.ascii_lowercase
        letters=string.ascii_letters
        return ''.join(random.choice(letters) for i in range(stringLength))



    def my_random_string(self, string_length=10):
        """Returns a random string of length string_length."""

    # Convert UUID format to a Python string.
        random = str(uuid.uuid4())

    # Make all characters uppercase.
        random = random.upper()

    # Remove the UUID '-'.
        random = random.replace("-","")

    # Return the random string.
        return random[0:string_length]



    def randString(self, length=5):
        #put your letters in the following string
        your_letters='abcdefghiABCVNMKLOPU1234567890'
        return ''.join((random.choice(your_letters) for i in range(length)))


    def randomStringDigits(self, stringLength=6):
        """Generate a random string of letters and digits """
        lettersAndDigits = string.ascii_letters + string.digits
        return ''.join(random.choice(lettersAndDigits) for i in range(stringLength))
