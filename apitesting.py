import csv

import requests
import json

# import data
import csv

from attr import attributes
from openpyxl.drawing import line

import data

"""def test_api_get():
    resp = requests.get( "https://reqres.in/api/users?page=2" )
    assert (resp.status_code == 200), "Status code is not 200. Rather found : " + str( resp.status_code )
    for record in resp.json()['data']:
        if record['id'] == 4:
            assert record['first_name'] == "Eve", \
                "Data not matched! Expected : Eve, but found : " + str( record['first_name'] )
            assert record['last_name'] == "Holt", \
                "Data not matched! Expected : Holt, but found : " + str( record['last_name'] )
    print( resp )"""

"""def test_api_post():
    data1 = {'name': 'John',
             'job': 'QA'}
    resp = requests.post( url="https://reqres.in/api/users", data=data1 )
    data1 = resp.json()
    assert (resp.status_code == 201), "Status code is not 201. Rather found : " \
                                      + str( resp.status_code )
    assert data1['name'] == "John", "User created with wrong name. \
        Expected : John, but found : " + str( data1['name'] )
    assert data1['job'] == "QA", "User created with wrong job. \
        Expected : QA, but found : " + str( data1['name'] )
    print( resp.text )
    data1 = resp.text
    with open( 'output.csv', 'w+' ) as f:
        f.write( resp.text )"""

## Write API Results to CSV
"""with open( 'E:\\Desktop\\testCompletionReport.csv', "wb" ) as csvFile:
    writer = csv.writer( csvFile, delimiter=',' )
    for line in data:
        writer.writerow( line )"""

"""url = "http://httpbin.org/post"

payload = "{\n    \"key1\": 1,\n    \"key2\": \"value2\"\n}"
headers = {
    'Content-Type': "application/json,text/plain",
    'User-Agent': "PostmanRuntime/7.15.0",
    'Accept': "*/*",
    'Cache-Control': "no-cache",
    'Postman-Token': "e908a437-88ea-4b00-af53-7a9a49033830,ba90e008-0f7f-4576-beb8-b7739c8961f1",
    'Host': "httpbin.org",
    'accept-encoding': "gzip, deflate",
    'content-length': "42",
    'Connection': "keep-alive",
    'cache-control': "no-cache"
    }

response = requests.request("POST", url, data=payload, headers=headers)
print(response)
print(response.text)"""

##def test_post_headers_body_json():
"""url = 'https://httpbin.org/post'
# Additional headers.
headers = {'Content-Type': 'application/json'}
# Body
payload = {'key1': 1, 'key2': 'value2'}
# convert dict to json by json.dumps() for body data.
resp = requests.post( url, headers=headers, data=json.dumps( payload, indent=4 ) )
# Validate response headers and body contents, e.g. status code.
assert resp.status_code == 200
resp_body = resp.json()
assert resp_body['url'] == url
# print response full body as text
print( resp.text )
print(resp)"""


def pretty_print_request(request):
    print( '\n{}\n{}\n\n{}\n\n{}\n'.format(
        '-----------Request----------->',
        request.method + ' ' + request.url,
        '\n'.join( '{}: {}'.format( k, v ) for k, v in request.headers.items() ),
        request.body )
    )


def pretty_print_response(response):
    print( '\n{}\n{}\n\n{}\n\n{}\n'.format(
        '<-----------Response-----------',
        'Status code:' + str( response.status_code ),
        '\n'.join( '{}: {}'.format( k, v ) for k, v in response.headers.items() ),
        response.text )
    )


# def test_post_headers_body_json():
url = 'https://httpbin.org/post'

# Additional headers.
headers = {'Content-Type': 'application/json'}

# Body
payload = {'key1': 1, 'key2': 'value2'}

# convert dict to json by json.dumps() for body data.
resp = requests.post( url, headers=headers, data=json.dumps( payload, indent=4 ) )

# Validate response headers and body contents, e.g. status code.
assert resp.status_code == 200
resp_body = resp.json()
assert resp_body['url'] == url
with open( 'output.csv', 'w+' ) as f:
    # f.write(resp)
    # f.writerow(line)
    f.write( resp.text )
    f.close()


# print full request and response
pretty_print_request( resp.request )
pretty_print_response( resp )

# This bit of code will write the result of the query to output.csv
