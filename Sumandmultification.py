def getProduct(n):
    product = 1
    while n != 0:
        product = product * (n % 10)
        n = n // 10
    return product
n = 555
print( getProduct( n ) )
def getSum(n):
    sum = 0
    # Single line that calculates sum
    while n > 0:
        sum += int( n % 10 )
        n = int( n / 10 )
    return sum
n = 555
print( getSum( n ) )
