from stringsumofnumbers import check


def findSum(str1):
    temp = ""
    Sum = 0
    for ch in str1:
        if (ch.isdigit()):
            temp += ch
        else:
            Sum += int( temp )
            temp = "0"
    return Sum + int( temp )
str1 = "12abc20yz68"
print( findSum( str1 ) )

# each integer value
def sum_digits_string(str1):

    sum_digit = 0
    for x in str1:
        if x.isdigit() == True:
            z = int( x )
            sum_digit = sum_digit + z

    return sum_digit


print( sum_digits_string( "123abcd45" ) )
print( sum_digits_string( "abcd1234" ) )
def check(my_string):
    brackets = ['()', '{}', '[]']
    while any( x in my_string for x in brackets ):
        for br in brackets:
            my_string = my_string.replace( br, '' )
    return not my_string
string = "{[]()}"
print( string, "-", "Balanced"
if check( string ) else "Unbalanced" )


def isPalindrome(s):
    return s == s[::-1]
# Driver code
s = "malayalam"
ans = isPalindrome( s )
if ans:
    print( "Yes" )
else:
    print( "No" )


def getProduct(n):
    product = 1
    while (n != 0):
        product = product * (n % 10)
        n = n // 10
    return product
    n = 4513
    print( getProduct( n ) )